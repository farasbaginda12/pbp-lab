from lab_1.models import Friend
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class DOB(forms.Form):
    dob = forms.DateField(widget=DateInput)

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        widgets = {'dob' : DateInput()}