from django.db import models

# Create your models here.
class Note(models.Model):
    from_var = "from"
    to = models.CharField(max_length=25)
    locals()[from_var] = models.CharField(max_length=25)
    title = models.CharField(max_length=25)
    message = models.TextField()
