# Generated by Django 3.2.7 on 2021-10-10 19:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0003_auto_20210927_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='message',
            field=models.TextField(),
        ),
    ]
