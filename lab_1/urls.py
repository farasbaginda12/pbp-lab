from django.urls import path, include
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    path('friends_list', friend_list, name='friends')
]
