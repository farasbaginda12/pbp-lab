from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse


# Create your views here.
def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab5_index.html', response)