## Apakah perbedaan antara JSON dan XML?

XML dengan JSON memiliki perbedaan yang fundnamental diantara keduanya yang ditandai dengan:
- Apabila dilihat dari segi penyimpanan data, JSON menerapakan penyimpanan data berbentuk seperti map dengan ditandai adanya key dan value sedangkan XML menerapkan metode penyimpanan data sebagai tree structure

- JSON memiliki kode data yang lebih sederhana yang membuat file JSON tergolong lebih ringan daripada XML. Hal itu ditandai salah satunya pada waktu pengiriman datanya akan lebih cepat dilakukan pada JSON dibandingkan XML.

- Apabila dilihat dari segi pengolahan data, JSON tidak dapat melakukan melakukan pemrosesan serta pemformatan dokumen dan objek, sedangkan pada XML hal ini dapat dilakukan.

- JSON hanya mendukung objek dalam tipe data primitif seperti string, integer, boolean, dan lainnya sedangkan pada XML mendukung banyak tipe data yang lebih kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya.

## Apakah perbedaan antara HTML dan XML?

Beberapa parameter berbeda untuk membandingkan perbedaan antara HTML dan XML diantaranya:

- HTML adalah bahasa markup standar untuk membuat halaman web dan aplikasi web. XML adalah bahasa markup yang mendefinisikan seperangkat aturan untuk menyandikan dokumen dalam format yang bisa dibaca manusia dan mesin.

- DIlihat dari segi set Tag, HTML memiliki tag yang sudah ditentukan sebelumnya. Di sisi lain XML mengharuskan kita yang menentukan set tagnya sendiri.

- Tag dalam XML yang sebelumnya dibuat wajib ditutup supaya tidak menimbulkan error pada saat pemanggilan, sedangkan dalam HTML tag terbuka atau bisa dikatakan tanpa perlu dititip juga dapat berfungsi dengan baik.

- Dilihat dari segi pemakaiannya,  HTML dan XML memiliki fokus yang jelas berbeda. HTML lebih berfokus pada menampilkan data sementara XML berfokus pada transport data antara aplikasi dan database.