from django.urls import path, include
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add'),
    path('note-list', note_list, name='note-list'),
]
