from django.http.response import  HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm

from django.core import serializers


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    form = NoteForm(request.POST or None, request.FILES or None)
      
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')
        
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)

'''
def xml(data):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(data):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
'''
